/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package org.mozilla.fenix.tor.interactor

import org.mozilla.fenix.tor.controller.TorBootstrapController

interface TorBootstrapInteractor {
    /**
     * Initiates Tor bootstrapping. Called when a user clicks on the "Connect" button.
     */
    fun onTorBootstrapConnectClicked()

    /**
     * Initiates Tor bootstrapping. Called when a user clicks on the "Connect" button.
     */
    fun onTorStartBootstrapping()

    /**
     * Stop Tor bootstrapping. Called when a user clicks on the "settings" cog/button.
     */
    fun onTorStopBootstrapping()

    /**
     * Initiates Tor bootstrapping with debug logging. Called when bootstrapping fails with
     * the control.txt file not existing.
     */
    fun onTorStartDebugBootstrapping()

    /**
     * Open Tor Network Settings preference screen
     */
    fun onTorBootstrapNetworkSettingsClicked()
}

class DefaultTorBootstrapInteractor(
    private val controller: TorBootstrapController,
) : TorBootstrapInteractor {

    override fun onTorBootstrapConnectClicked() {
        controller.handleTorBootstrapConnectClicked()
    }

    override fun onTorStopBootstrapping() {
        controller.handleTorStopBootstrapping()
    }

    override fun onTorStartBootstrapping() {
        controller.handleTorStartBootstrapping()
    }

    override fun onTorStartDebugBootstrapping() {
        controller.handleTorStartDebugBootstrapping()
    }

    override fun onTorBootstrapNetworkSettingsClicked() {
        controller.handleTorNetworkSettingsClicked()
    }
}
